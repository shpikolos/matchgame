﻿using System.Web;
using System.Web.Optimization;

namespace MatchGame
{
	public class BundleConfig
	{
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle(BundleConstants.JQUERY).Include(Constants.JQUERY_JS_PROJECT_JS_URL));
			bundles.Add(new ScriptBundle(BundleConstants.SCRIPTS).Include(Constants.MATCH_JS_PROJECT_URL));
			bundles.Add(new StyleBundle(BundleConstants.STYLES_CSS).Include(Constants.SITE_CSS_PROJECT_URL));
			bundles.Add(new StyleBundle(BundleConstants.THEME_BASE_STYLES_CSS).Include(
						"~/Content/themes/base/jquery.ui.core.css",
						"~/Content/themes/base/jquery.ui.resizable.css",
						"~/Content/themes/base/jquery.ui.selectable.css",
						"~/Content/themes/base/jquery.ui.accordion.css",
						"~/Content/themes/base/jquery.ui.autocomplete.css",
						"~/Content/themes/base/jquery.ui.button.css",
						"~/Content/themes/base/jquery.ui.dialog.css",
						"~/Content/themes/base/jquery.ui.slider.css",
						"~/Content/themes/base/jquery.ui.tabs.css",
						"~/Content/themes/base/jquery.ui.datepicker.css",
						"~/Content/themes/base/jquery.ui.progressbar.css",
						"~/Content/themes/base/jquery.ui.theme.css"));
		}
	}
}