﻿var Match = function (settings) {
    if (_match == null)
        _match = this;
    else
        return _match;
    //  Настройки контролла.
    this.Settings = settings;
    //  Контроллы.
    this.Controls = {}
    this.Controls.Start = null;
    this.Controls.Save = null;
    this.Controls.Main = null;
    this.Controls.InfoBlock = null;
    this.Controls.ScoreText = null;
    this.Controls.Score = null;
    this.Controls.StepsText = null;
    this.Controls.Steps = null;
    this.Controls.GameField = null;
    this.Controls.SavedGames = null;
    //  Объект с данными текущего объекта.
    this.ClientData = {}
    this.ClientData.PostName = "POST";
    this.ClientData.StartUrl = "/match/startgame";
    this.ClientData.LoadUrl = "/match/loadgame";
    this.ClientData.CheckUrl = "/match/dostep";
    this.ClientData.SaveGameUrl = "/savedgames/save";
    this.ClientData.FirstField = null;
    this.ClientData.ScoreText = "Счёт:";
    this.ClientData.StepsText = "Количество ходов:";
    this.ClientData.Confirm = "Текущая игра будет потеряна. Вы уверены?";
    this.ClientData.Steps = 0;
    this.ClientData.Links = new Array();
}
Match.prototype = {
    CheckFields: function (field1, field2) {
        var param1 = this.GetValidValue("field1", field1);
        var param2 = this.GetValidValue("field2", field2);
        var postData = param1 + "&" + param2;
        this.Request(this.ClientData.PostName, this.ClientData.CheckUrl, postData,
            function (t) {
                return function (data) {
                    t.DoGameSucceeded(data);
                }
            }(this),
            function (t) {
                return function (error) {
                    t.DoGameFailed(error);
                }
            }(this));
        this.ClientData.FirstField = null;
    },
    DoGame: function (method, url, data) {
        if (this.ClientData.Steps == 0 || confirm(this.ClientData.Confirm)) {
            this.Controls.InfoBlock.style.display = "block";
            this.Request(method, url, data,
				function (t) {
				    return function (data) {
				        t.DoGameSucceeded(data);
				    }
				}(this),
				function (t) {
				    return function (error) {
				        t.DoGameFailed(error);
				    }
				}(this));
        }
    },
    DoGameFailed: function (error) {
        console.log(error);
        alert('Ошибка');
    },
    DoGameSucceeded: function (data) {
        this.Controls.Save.style.display = "inline-block";
        this.Controls.Score.innerHTML = data.Score;
        this.Controls.Steps.innerHTML = data.Steps;
        this.Controls.GameField.innerHTML = "";
        var currentRow = 0;
        var row = this.GetRowCtrl();
        this.Controls.GameField.appendChild(row);
        for (var i = 0; i < data.Fields.length; i++) {
            var field = this.GetFieldCtrl(data.Fields[i]);
            if (data.Fields[i].X != currentRow) {
                row = this.GetRowCtrl();
                this.Controls.GameField.appendChild(row);
                currentRow = data.Fields[i].X;
            }
            row.appendChild(field);
        }
        this.ClientData.Steps = data.Steps;
    },
    GetEmptyCtrl: function (field) {
        var div = document.createElement("div");
        div.setAttribute("clr", "-1");
        field.Ctrl = div;
        return div;
    },
    GetFieldCtrl: function (field) {
        var fieldInfo = this.GetFieldInfo(field.ID);
        var div = document.createElement("div");
        div.setAttribute("title", fieldInfo.Title);
        div.style.backgroundColor = fieldInfo.ColorCss;
        div.onclick = function (t, f) {
            return function () {
                t.SelectField(f)
            }
        }(this, field)
        field.Ctrl = div;
        return div;
    },
    GetFieldInfo: function (id) {
        return this.Settings.FieldTypes[this.ClientData.Links[id]];
    },
    GetRowCtrl: function () {
        var div = document.createElement("div");
        div.setAttribute("class", "field-row");
        return div;
    },
    GetValidValue: function (key, value) {
        var result = key + "=" + encodeURIComponent(JSON.stringify(value));
        return result;
    },
    Init: function () {
        this.Controls.SavedGames = document.getElementById(this.Settings.SavedGamesId);
        this.Controls.Save = document.getElementById(this.Settings.SaveId);
        this.Controls.Save.onclick = function (t) {
            return function () {
                t.Save();
            }
        }(this);
        this.Controls.Start = document.getElementById(this.Settings.StartId);
        this.Controls.Start.onclick = function (t) {
            return function () {
                t.Start();
            }
        }(this);
        this.Controls.Main = document.getElementById(this.Settings.GameFieldId);
        this.Controls.InfoBlock = document.createElement("div");
        this.Controls.InfoBlock.setAttribute("class", "match-field");
        this.Controls.InfoBlock.style.display = "none";
        this.Controls.ScoreText = document.createElement("span");
        this.Controls.ScoreText.setAttribute("class", "match-field-score");
        this.Controls.ScoreText.innerHTML = this.ClientData.ScoreText;
        this.Controls.Score = document.createElement("span");
        this.Controls.StepsText = document.createElement("span");
        this.Controls.StepsText.setAttribute("class", "match-field-stepstext");
        this.Controls.StepsText.innerHTML = this.ClientData.StepsText;
        this.Controls.Steps = document.createElement("span");
        this.Controls.Steps.setAttribute("class", "match-field-steps");
        this.Controls.InfoBlock.appendChild(this.Controls.ScoreText);
        this.Controls.InfoBlock.appendChild(this.Controls.Score);
        this.Controls.InfoBlock.appendChild(this.Controls.Steps);
        this.Controls.InfoBlock.appendChild(this.Controls.StepsText);
        var center = document.createElement("div");
        center.setAttribute("class", "match-center-position");
        this.Controls.GameField = document.createElement("div");
        center.appendChild(this.Controls.GameField);
        this.Controls.Main.appendChild(this.Controls.InfoBlock);
        this.Controls.Main.appendChild(center);
        this.ClientData.Links = new Array();
        for (var i = 0; i < this.Settings.FieldTypes.length; i++)
            this.ClientData.Links[this.Settings.FieldTypes[i].ID] = i;
    },
    Load: function (id) {
        var url = this.ClientData.LoadUrl + "/" + id;
        this.DoGame(this.ClientData.PostName, url, null);
    },
    Request: function (method, url, body, onSuccess, onFailure) {
        var req = null;
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                req = null;
            }
        }
        if (req == null && (typeof (XMLHttpRequest) != "undefined"))
            req = new XMLHttpRequest();
        if (req == null) {
            console.error("Can't get XHR object.")
            return;
        }
        req.open(method, url, true);
        req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        req.onreadystatechange = function (myReq, myOnSuccess, myOnFailure) {
            return function () {
                if (myReq.readyState == 4) {
                    if (myReq.status == 200) {
                        var cnt = myReq.getResponseHeader("content-type");
                        var result = null;
                        if (!!myReq.responseText)
                            result = myReq.responseText;
                        else
                            result = myReq.response;
                        if (cnt != undefined && cnt != null && cnt.indexOf("application/json") >= 0)
                            myOnSuccess(JSON.parse(result));
                        else
                            myOnSuccess(result);
                    }
                    else {
                        myOnFailure(!!myReq.responseText ? myReq.responseText : myReq.response);
                    }
                }
            }
        }(req, onSuccess, onFailure);
        req.send(body);
    },
    Save: function () {
        this.Request(this.ClientData.PostName, this.ClientData.SaveGameUrl, null,
            function (t) {
                return function (data) {
                    t.SaveSucceeded(data);
                }
            }(this),
            function (t) {
                return function (error) {
                    t.SaveFailed(error);
                }
            }(this));
    },
    SaveFailed: function (error) {
        console.log(error);
    },
    SaveSucceeded: function (data) {
        this.Controls.SavedGames.innerHTML = data;
    },
    SelectField: function (fieldInfo) {
        if (this.ClientData.FirstField == null) {
            this.ClientData.FirstField = fieldInfo;
            fieldInfo.Ctrl.style.border = "3px solid #FF0000";
        }
        else if (this.ClientData.FirstField == fieldInfo) {
            this.ClientData.FirstField = null;
            fieldInfo.Ctrl.style.border = "";
        }
        else {
            this.CheckFields(this.ClientData.FirstField, fieldInfo);
        }
    },
    Start: function () {
        this.DoGame(this.ClientData.PostName, this.ClientData.StartUrl, null);
    }
}
var MatchSettings = function () {
    this.StartId = null;
    this.GameFieldId = null;
    this.SaveId = null;
    this.SavedGamesId = null;
    this.FieldTypes = null;
}
var _match = null;