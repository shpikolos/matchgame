﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatchGame.Interfaces
{
	public interface IField
	{
		int X { get; set; }
		int Y { get; set; }
		string CssClass { get; set; }
	}
}