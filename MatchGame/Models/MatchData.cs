﻿using MatchGame.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace MatchGame.Models
{
    [DataContract]
    [Serializable]
    public class MatchData
    {
        public MatchData()
        {
        }

        public const string MATCH_DATA_SESSION_NAME = "MatchData";
        [XmlIgnore]
        public const int DEFAULT_WIDTH = 10;
        [XmlIgnore]
        public const int DEFAULT_HEIGHT = 10;
        [XmlIgnore]
        public const int FIELD_SCORE = 10;

        /// <summary>
        /// Проставляется только при сохранении.
        /// </summary>
        [XmlAttribute("ID")]
        public Guid ID { get; set; }
        [XmlAttribute("Steps")]
        public int Steps { get; set; }
        [XmlAttribute("Score")]
        public int Score { get; set; }
        [XmlAttribute("Width")]
        public int Width { get; set; }
        [XmlAttribute("Height")]
        public int Height { get; set; }
        [XmlElement("Fields")]
        public Field[] Fields { get; set; }

        /// <summary>
        /// Обновляет таблицу с учётом выбора пользователя.
        /// </summary>
        /// <param name="field1">Клетка для замены.</param>
        /// <param name="field2">Клетка с которой менять.</param>
        public void Refresh(Field field1, Field field2)
        {
            if (this.CheckOneDistance(field1, field2))
            {
                this.Steps++;
                // Производится обмен строк.
                Field[,] fields = this.GetMatrix();
                int x = field1.X;
                int y = field1.Y;
                fields[field1.X, field1.Y] = field2;
                fields[field2.X, field2.Y] = field1;
                field1.X = field2.X;
                field1.Y = field2.Y;
                field2.X = x;
                field2.Y = y;

                //  В строке первой выбранной клетки производится проверка стоящих рядом друг с другом
                //  клеток одного цвета. Если стоящих клеток больше двух, производится удаление части 
                //  строки. 
                DeleteIndexes(ref fields, field1.X);
                //  Аналогичная проверка для строки где находится вторая клетка.
                DeleteIndexes(ref fields, field2.X);

                this.Fields = GetArray(fields);
            }

        }

        private Field[] GetArray(Field[,] fields)
        {
            Field[] result = new Field[this.Height * this.Width];
            for (int i = 0; i < this.Width; i++)
                for (int j = 0; j < this.Height; j++)
                    result[i * this.Height + j] = fields[i, j];
            return result;
        }
        private Field[,] GetMatrix()
        {
            Field[,] result = new Field[this.Width, this.Height];
            for (int i = 0; i < this.Fields.Length; i++)
            {
                Field field = this.Fields[i];
                result[field.X, field.Y] = field;
            }
            return result;
        }
        /// <summary>
        /// Возвращает значение, показывающее стоят ли две выбранные клетки рядом друг с другом по вертикали или горизонтали.
        /// </summary>
        /// <param name="field1">Первая клетка.</param>
        /// <param name="field2">Вторая клетка.</param>
        /// <returns></returns>
        private Boolean CheckOneDistance(Field field1, Field field2)
        {
            if (
                (field1.X == field2.X && Math.Abs(field1.Y - field2.Y) == 1) ||
                (field1.Y == field2.Y && Math.Abs(field1.X - field2.X) == 1))
                return true;
            else
                return false;
        }
        private void DeleteIndexes(ref Field[,] fields, int y)
        {
            if (this.Width > 2)
            {
                List<int> forDelete = new List<int>();
                for (int i = 2; i < this.Width; i++)
                {
                    if (fields[y, i - 2].ID == fields[y, i - 1].ID && fields[y, i - 1].ID == fields[y, i].ID)
                        forDelete.AddRange(new int[] { i - 2, i - 1, i });
                }
                int[] d = (from fd in forDelete
                           orderby fd ascending
                           select fd).Distinct().ToArray();

                if (d.Length > 0)
                {
                    int additionalScores = 0;
                    FieldInfo fieldInfo = FieldInfo.GetCachedField(fields[y, d[0]].ID);
                    if (fieldInfo != null)
                        additionalScores = fieldInfo.AdditionalBonus * d.Length;
                    //  Удаление строки со смещением всех строк вниз.
                    RemovePartOfRaw(ref fields, y, d[0], d[d.Length - 1]);
                    //  Заполнение самой верхней строки рандомными клетками.
                    FillRawByRandomFields(ref fields, 0, d[0], d[d.Length - 1]);

                    this.Score += d.Length * FIELD_SCORE + GetScoreBonus(d.Length) + additionalScores;
                }
            }
        }
        /// <summary>
        /// Удаляет строку, возвращая дополнительные очки клетки.
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="rowIndex"></param>
        /// <param name="startColumnIndex"></param>
        /// <param name="endColumnIndex"></param>
        /// <returns></returns>
        private void RemovePartOfRaw(ref Field[,] fields, int rowIndex, int startColumnIndex, int endColumnIndex)
        {
            for (int i = rowIndex; i > 0; i--)
            {
                for (int j = startColumnIndex; j <= endColumnIndex; j++)
                {
                    fields[i, j].ID = fields[i - 1, j].ID;
                }
            }
        }
        private void FillRawByRandomFields(ref Field[,] fields, int rowIndex, int startColumnIndex, int endColumnIndex)
        {
            FieldInfo[] fieldInfos = FieldInfo.GetFields();
            Random random = new Random(DateTime.Now.Millisecond);
            Field preventField = null;
            for (int j = startColumnIndex; j < endColumnIndex; j++)
            {
                if (j == 0)
                    preventField = null;
                else
                    preventField = fields[rowIndex, j - 1];
                FieldInfo fieldInfo = fieldInfos[random.Next(0, fieldInfos.Length)];
                while (preventField != null && preventField.ID == fieldInfo.ID)
                    fieldInfo = fieldInfos[random.Next(0, fieldInfos.Length)];
                fields[rowIndex, j] = new Field() { X = rowIndex, Y = j, ID = fieldInfo.ID };
            }
        }
        /// <summary>
        /// Возвращает бонус счёт в зависимости от количества элементов для удаления.
        /// </summary>
        /// <param name="countForDelete">Количество элементов для удаления.</param>
        /// <returns></returns>
        private int GetScoreBonus(int countForDelete)
        {
            if (countForDelete < 4)
                return 0;
            else if (countForDelete == 4)
                return FIELD_SCORE * 2;
            else
                return FIELD_SCORE * 10;
        }

        public static MatchData GetRandomGame()
        {
            return GetRandomGame(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        }
        public static MatchData GetRandomGame(int width, int height)
        {
            MatchData game = new MatchData();
            game.Width = width;
            game.Height = height;
            game.Fields = new Field[width * height];
            FieldInfo[] fieldInfos = FieldInfo.GetFields();
            Random random = new Random(DateTime.Now.Millisecond);

            Field preventField = null;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (j == 0)
                        preventField = null;
                    else
                        preventField = game.Fields[i * height + j - 1];
                    FieldInfo fieldInfo = fieldInfos[random.Next(0, fieldInfos.Length)];
                    while (preventField != null && preventField.ID == fieldInfo.ID)
                        fieldInfo = fieldInfos[random.Next(0, fieldInfos.Length)];
                    game.Fields[i * height + j] = new Field() { X = i, Y = j, ID = fieldInfo.ID };
                }
            }
            return game;
        }
        public static MatchData GetFromSession(HttpSessionStateBase session)
        {
            MatchData matchData = (MatchData)session[MATCH_DATA_SESSION_NAME];
            return matchData;
        }
        public static void SetToSession(HttpSessionStateBase session, MatchData matchData)
        {
            session[MatchData.MATCH_DATA_SESSION_NAME] = matchData;
        }
        public static void ClearSession(HttpSessionStateBase session)
        {
            session.Remove(MatchData.MATCH_DATA_SESSION_NAME);
        }
    }

    [DataContract]
    [Serializable]
    public class Field
    {
        public Field()
        {
        }
        public int X { get; set; }
        public int Y { get; set; }
        /// <summary>
        /// Тип клетки
        /// </summary>
        public int ID { get; set; }
    }

    public class FieldBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            Field field = null;
            try
            {
                string json = controllerContext.HttpContext.Request.Form[bindingContext.ModelName];
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                field = serializer.Deserialize<Field>(json);
            }
            catch
            {
                field = null;
            }
            return field;
        }
    }

    public class MatchActionResult : ActionResult
    {
        public MatchActionResult()
        {

        }
        public MatchActionResult(MatchData matchData)
        {
            this.m_matchData = matchData;
        }

        private MatchData m_matchData = null;
        public MatchData MatchData
        {
            get { return this.m_matchData; }
            set { this.m_matchData = value; }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.Output.Write(serializer.Serialize(this.m_matchData));
            context.HttpContext.Response.End();
        }
    }

}