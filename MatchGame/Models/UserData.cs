﻿using MatchGame.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MatchGame.Models
{
    public class UserData
    {
        [Display(Name = MatchGame.Textes.LOGIN_NAME_TEXT)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Textes.LOGIN_EMPTY_VALUE)]
        public string Login { get; set; }

        [Display(Name = MatchGame.Textes.PASSWORD_NAME_TEXT)]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Textes.PASSWORD_EMPTY_VALUE)]
        public string Password { get; set; }

        public static Boolean CheckUser(string login, string password)
        {
            return CheckUser(login, password, false);
        }
        public static Boolean CheckUser(string login, string password, Boolean withoutCache)
        {
            UserData[] users = GetUsers(withoutCache);
            if (users != null)
            {
                UserData[] user = (from u in users where u.Login == login && u.Password == password select u).ToArray();
                if (user != null && user.Length > 0)
                    return true;
            }
            return false;
        }
        public static Boolean IsExistLogin(string login)
        {
            return IsExistLogin(login, false);
        }
        public static Boolean IsExistLogin(string login, Boolean withoutCache)
        {
            UserData[] users = GetUsers(withoutCache);
            if (users != null)
            {
                UserData[] user = (from u in users where u.Login == login select u).ToArray();
                if (user != null && user.Length > 0)
                    return true;
            }
            return false;
        }
        public static UserData[] GetUsers()
        {
            return GetUsers(false);
        }
        public static UserData[] GetUsers(Boolean withoutCache)
        {
            UserData[] users = null;
            object cached = null;
            if (withoutCache)
                CacheHelper.Remove(CacheKeys.USERS);
            else
                cached = CacheHelper.Get(CacheKeys.USERS);
            if (cached == null)
            {
                users = GetUsersFromDataSource();
                if (users != null)
                    CacheHelper.Add(users, CacheKeys.USERS);
            }
            else
            {
                users = cached as UserData[];
            }
            return users;
        }
        public static Boolean AddUser(UserData user)
        {
            ClearCache();
            return AddUserToDataSource(user);
        }

        private static Boolean AddUserToDataSource(UserData user)
        {
            try
            {
                string path = GetXmlPath();
                List<UserData> users = GetUsersFromDataSource().ToList();
                users.Add(user);
                UserData[] usersForSave = users.ToArray();
                string xml = CommonHelper.SerializeWithIndent<UserData[]>(usersForSave);
                System.IO.File.WriteAllText(path, xml);
                return true;
            }
            catch
            {

            }
            return false;
        }
        private static UserData[] GetUsersFromDataSource()
        {
            try
            {
                string path = GetXmlPath();
                UserData[] data = CommonHelper.Deserialize<UserData[]>(System.IO.File.ReadAllText(path));
                return data;
            }
            catch
            {
                return new UserData[0];
            }
        }
        private static string GetXmlPath()
        {
            return System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, @"App_data\Users.xml");
        }
        private static void ClearCache()
        {
            CacheHelper.Clear();
        }
    }
}