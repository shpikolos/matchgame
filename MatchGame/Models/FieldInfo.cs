﻿using MatchGame.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace MatchGame.Models
{
    /// <summary>
    /// Описание клетки.
    /// </summary>
    [Serializable]
    [DataContract]
    public class FieldInfo
    {
        [XmlIgnore]
        private const string USUAL_FIELD_TITLE_TEXT = "Обычная клетка";
        [XmlIgnore]
        private const string FIELDS_CACHE_KEY = "FieldInfo";

        public FieldInfo()
        {
            this.CanMove = true;
            this.CanHorizontalMove = true;
            this.CanVarticalMove = true;
            this.Title = USUAL_FIELD_TITLE_TEXT;
        }

        /// <summary>
        /// Уникальный ID типа клетки.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public int ID { get; set; }
        /// <summary>
        ///  Цвет клетки в формате RGB (#FFFFFF - белый).
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public string ColorCss { get; set; }
        /// <summary>
        /// Можно ли перемещать клетку мышкой.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public Boolean CanMove { get; set; }
        /// <summary>
        /// Можно ли перемещать клетку мышкой по вертикали.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public Boolean CanHorizontalMove { get; set; }
        /// <summary>
        /// Можно ли перемещать клетку мышкой по горизонтали.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public bool CanVarticalMove { get; set; }
        /// <summary>
        /// Текст всплывающей подсказки по наведению на клетку.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public string Title { get; set; }
        /// <summary>
        /// Дополнительный бонус при удалении клетки.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public int AdditionalBonus { get; set; }

        /// <summary>
        /// Возвращает список полей по умолчанию.
        /// </summary>
        /// <returns></returns>
        public static FieldInfo[] GetDefaultFields()
        {
            FieldInfo[] fields = new FieldInfo[]
            {
                new FieldInfo() {ID = 0, ColorCss =  "#A70105"},
                new FieldInfo() {ID = 1, ColorCss =  "#00FF00"},
                new FieldInfo() {ID = 2, ColorCss =  "#0000FF"},
                new FieldInfo() {ID = 3, ColorCss =  "#EBACCA"},
                new FieldInfo() {ID = 4, ColorCss =  "#99D9EA"},
                new FieldInfo() {ID = 5, ColorCss =  "#008000"},
                new FieldInfo() {ID = 6, ColorCss =  "#800000"},
                new FieldInfo() {ID = 7, ColorCss =  "#808000"}
            };
            return fields;
        }
        public static FieldInfo[] GetFields()
        {
            return GetFieldsFromXml();
        }
        public static string GetAsJson(FieldInfo[] data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(data);
            return json;
        }
        public static FieldInfo GetCachedField(int id)
        {
            FieldInfo[] fields = GetCachedFields();
            FieldInfo[] flds = (from f in fields where f.ID == id select f).ToArray();
            if (flds.Length > 0)
                return flds[0];
            return null;
        }
        public static FieldInfo[] GetCachedFields()
        {
            FieldInfo[] fields = null;
            object obj = CacheHelper.Get(FIELDS_CACHE_KEY);
            if (obj == null)
            {
                fields = FieldInfo.GetFields();
                CacheHelper.Add(fields, FIELDS_CACHE_KEY);
            }
            else
            {
                fields = obj as FieldInfo[];
            }
            return fields;
        }

        private static FieldInfo[] GetFieldsFromXml()
        {
            try
            {
                string path = GetXmlPath();
                string xml = System.IO.File.ReadAllText(path);
                FieldInfo[] data = null;
                if (string.IsNullOrEmpty(xml))
                {
                    data = GetDefaultFields();
                    SaveFieldsToXml(data);
                }
                else
                {
                    data = CommonHelper.Deserialize<FieldInfo[]>(xml);
                }
                return data;
            }
            catch
            {
                FieldInfo[] data = GetDefaultFields();
                SaveFieldsToXml(data);
                return data;
            }
        }
        private static void SaveFieldsToXml(FieldInfo[] data)
        {
            string path = GetXmlPath();
            string xml = CommonHelper.SerializeWithIndent<FieldInfo[]>(data);
            System.IO.File.WriteAllText(path, xml);
        }
        private static string GetXmlPath()
        {
            return System.IO.Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, @"App_data\FieldInfo.xml");
        }
    }
}
