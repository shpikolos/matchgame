﻿using MatchGame.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MatchGame.Models
{
    /// <summary>
    /// Модель победителя.
    /// </summary>
    [Serializable]
    public class Winner
    {
        /// <summary>
        /// Максимальное количество людей в таблице победителей.
        /// </summary>
        [XmlIgnore]
        public const int MAX_WINNER_COUNT = 5;

        /// <summary>
        /// Логиг.
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Количество шагов.
        /// </summary>
        public int Steps { get; set; }
        /// <summary>
        /// Счёт.
        /// </summary>
        public int Score { get; set; }
        /// <summary>
        /// Место.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Добавляет победителя в таблицу победителей, если это возможно. Сохранение в xml файл.
        /// </summary>
        /// <param name="winner">Победитель.</param>
        private static Winner[] SaveWinnersToXml(Winner winner)
        {
            try
            {
                string path = GetXmlPath();
                List<Winner> winners = GetWinnersFromXml().ToList();
                winners.Add(winner);
                Winner[] newWinners = (from w in winners
                                       orderby w.Score descending
                                       select w).Take(MAX_WINNER_COUNT).ToArray();
                for (int i = 0; i < newWinners.Length; i++)
                    newWinners[i].Number = i + 1;
                string xml = CommonHelper.Serialize<Winner[]>(newWinners);
                System.IO.File.WriteAllText(path, xml);
                return newWinners;
            }
            catch
            {
                return new Winner[0];
            }
        }
        /// <summary>
        /// Возвращает список победителей. Загрузка из xml файла.
        /// </summary>
        /// <returns></returns>
        private static Winner[] GetWinnersFromXml()
        {
            try
            {
                string path = GetXmlPath();
                Winner[] data = CommonHelper.Deserialize<Winner[]>(System.IO.File.ReadAllText(path));
                return data;
            }
            catch
            {
                return new Winner[0];
            }
        }
        /// <summary>
        /// Возвращает путь xml файла с победителями в файловой системе.
        /// </summary>
        /// <returns></returns>
        private static string GetXmlPath()
        {
            return System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, @"App_data\Winners.xml");
        }

        /// <summary>
        /// Возвращает список победителей.
        /// </summary>
        /// <returns></returns>
        public static Winner[] GetWinners()
        {
            return GetWinnersFromXml();
        }
        /// <summary>
        /// Добавляет и сохраняет победителя.
        /// </summary>
        /// <param name="winner">Победитель.</param>
        /// <returns></returns>
        public static Winner[] SaveWinners(Winner winner)
        {
            return SaveWinnersToXml(winner);
        }
    }
}
