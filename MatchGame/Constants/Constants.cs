﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatchGame
{
    public class Constants
    {
        public const string JQUERY_JS_PROJECT_JS_URL = "~/Scripts/jquery-1.8.2.js";
        public const string MATCH_JS_PROJECT_URL = "~/Scripts/Match.js";
        public const string SITE_CSS_PROJECT_URL = "~/Content/site.css";
        public const int MINUTES_FOR_USERS_CACHE = 20;
    }
    public class BundleConstants
    {
        public const string JQUERY = "~/bundles/jquery";
        public const string SCRIPTS = "~/bundles/scripts";
        public const string STYLES_CSS = "~/Content/css";
        public const string THEME_BASE_STYLES_CSS = "~/Content/themes/base/css";
    }
    public class Textes
    {
        public const string LOGIN_EMPTY_VALUE = "Необходимо ввести логин";
        public const string PASSWORD_EMPTY_VALUE = "Необходимо ввести пароль";
        public const string INVALID_USER_DATA = "Неправильно введены логин или пароль";
        public const string LOGIN_EXISTS = "Пользователь с таким логином уже зарегистрирован";
        public const string SAVED_GAMES_NUMBER_COLUMN_TEXT = "№";
        public const string SAVED_GAMES_SCORE_COLUMN_TEXT = "Счёт";
        public const string SAVED_GAMES_STEPS_COLUMN_TEXT = "Кол-во шагов";
        public const string SAVED_GAMES_WIDTH_COLUMN_TEXT = "Кол-во строк";
        public const string SAVED_GAMES_HEIGHT_COLUMN_TEXT = "Кол-во столбцов";
        public const string LOAD_BUTTON_TEXT = "Загрузить";
        public const string LOGIN_NAME = "Имя";
        public const string WIN_POSITION = "Место";
        public const string WIN_TABLE_TEXT = "Таблица победителей";
        public const string PLAY_TOOLBAR_TEXT = "Игра";
        public const string WINNERS_TOOLBAR_TEXT = "Победители";
        public const string LOGIN_TEXT = "Войти";
        public const string LOGOUT_TEXT = "Выйти";
        public const string START = "Начать";
        public const string SAVE = "Сохранить";
        public const string END_GAME = "Завершить";
        public const string REGISTRATION_TEXT = "Регистрация";
        public const string REGISTRAT_TEXT = "Зарегистрироваться";
        public const string LOGIN_NAME_TEXT = "Логин";
        public const string PASSWORD_NAME_TEXT = "Пароль";
        public const string LOGGING_TEXT = "Вход в систему";
    }
    public class CacheKeys
    {
        public const string USERS = "users";
    }

}