﻿using MatchGame.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace MatchGame.Helpers
{
	public class CommonHelper
	{
		/// <summary>
		/// Сериализует данные без лишних пробелов, новых строк.
		/// </summary>
		/// <typeparam name="T">Тип данных.</typeparam>
		/// <param name="data">Данные для сериализации.</param>
		/// <returns></returns>
		public static string Serialize<T>(T data)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = false;
			settings.NewLineHandling = NewLineHandling.None;
			settings.NewLineOnAttributes = false;
			return Serialize<T>(data, settings);
		}
		public static string SerializeWithIndent<T>(T data)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.NewLineHandling = NewLineHandling.None;
			settings.NewLineOnAttributes = false;
			return Serialize<T>(data, settings);
		}
		public static string Serialize<T>(T data, XmlWriterSettings settings)
		{
			StringBuilder sb = new StringBuilder();
			using (XmlWriter writer = XmlWriter.Create(sb, settings))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(T));
				serializer.Serialize(writer, data);
				return sb.ToString();
			}
		}
		/// <summary>
		/// Десериализует данные.
		/// </summary>
		/// <typeparam name="T">Тип данных.</typeparam>
		/// <param name="xml">Xml для десериализации.</param>
		/// <returns></returns>
		public static T Deserialize<T>(string xml)
		{
			using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(T));
				return (T)serializer.Deserialize(reader);
			}
		}
	}
}