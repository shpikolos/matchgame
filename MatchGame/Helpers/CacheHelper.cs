﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MatchGame.Helpers
{
    public class CacheHelper
    {
        private const int MINUTES_FOR_CACHE = 20;

        /// <summary>
        /// Добавляет значение в кэш.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <param name="key">Ключ кэша.</param>
        public static void Add(object value, string key)
        {
            if (value != null)
                HttpRuntime.Cache.Add(key, value, null, DateTime.Now.AddMinutes(MINUTES_FOR_CACHE), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
        }
        /// <summary>
        /// Получает значение из кэша.
        /// </summary>
        /// <param name="key">Ключ кэша.</param>
        /// <returns></returns>
        public static object Get(string key)
        {
            return HttpRuntime.Cache.Get(key);
        }
        /// <summary>
        /// Удаляет значение из кэша.
        /// </summary>
        /// <param name="key">Ключ кэша.</param>
        public static void Remove(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }
        /// <summary>
        /// Очищает весь кэш.
        /// </summary>
        public static void Clear()
        {
            foreach (DictionaryEntry cacheItem in HttpRuntime.Cache)
                HttpRuntime.Cache.Remove(cacheItem.Key.ToString());
        }
    }
}
