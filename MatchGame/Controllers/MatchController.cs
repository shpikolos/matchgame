﻿using MatchGame.Helpers;
using MatchGame.Models;
using MatchGame.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MatchGame.Controllers
{
	[Authorize]
	public class MatchController : Controller
	{
		public ActionResult Main()
		{          
            return View();
		}

		[HttpPost]
		public ActionResult StartGame()
        {
            CacheHelper.Clear();
            MatchData matchData = MatchData.GetRandomGame();
			MatchData.SetToSession(Session, matchData);
			return new MatchActionResult(matchData);
		}
		[HttpPost]
		public ActionResult LoadGame(Guid id)
        {
            CacheHelper.Clear();
            MatchData matchData = MatchForSave.LoadFromXml(HttpContext.User.Identity.Name, id);
			MatchData.SetToSession(Session, matchData);
			return new MatchActionResult(matchData);
		}
		[HttpPost]
		public ActionResult DoStep([System.Web.Http.FromBody] Field field1, [System.Web.Http.FromBody] Field field2)
		{
			MatchData matchData = MatchData.GetFromSession(Session);
            if (matchData == null)
            {
                matchData = MatchData.GetRandomGame();
                MatchData.SetToSession(Session, matchData);
            }
			matchData.Refresh(field1, field2);
			return new MatchActionResult(matchData);
		}
	}
}
