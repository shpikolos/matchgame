﻿using MatchGame.Models;
using MatchGame.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatchGame.Controllers
{
	[Authorize]
	public class SavedGamesController : Controller
	{
		[HttpGet]
		public ActionResult Main()
		{
			MatchData[] games = MatchForSave.LoadFromXml(HttpContext.User.Identity.Name);
			return View(games);
		}

		[HttpPost]
		public ActionResult Save()
		{
			MatchData matchData = MatchData.GetFromSession(Session);
			if (matchData != null)
			{
				MatchForSave.Save(HttpContext.User.Identity.Name, matchData);
			}
			MatchData[] games = MatchForSave.LoadFromXml(HttpContext.User.Identity.Name);
			return View("Main", games);
		}
	}
}
