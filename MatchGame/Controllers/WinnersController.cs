﻿using MatchGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatchGame.Controllers
{
	[Authorize]
	public class WinnersController : Controller
	{
		[HttpGet]
		public ActionResult Main()
		{
			Winner[] winners = Winner.GetWinners();
			return View(winners);
		}
		[HttpGet]
		public ActionResult End()
		{
			MatchData data = MatchData.GetFromSession(Session);
			if (data != null)
			{
				Winner winner = new Winner()
				{
					Login = HttpContext.User.Identity.Name,
					Score = data.Score,
					Steps = data.Steps
				};
				Winner.SaveWinners(winner);
				MatchData.ClearSession(Session);
			}
			return RedirectToAction("Main");
		}
	}
}
