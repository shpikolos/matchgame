﻿using MatchGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatchGame.Controllers
{
	public class AccountController : Controller
	{
		[HttpGet]
		public ActionResult Register()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Register([System.Web.Http.FromBody] UserData userData)
		{
			if (ModelState.IsValid)
			{
				Boolean isExistLogin = UserData.IsExistLogin(userData.Login);
				if (isExistLogin)
				{
					ViewBag.IsExistLogin = true;
				}
				else
				{
					if (UserData.AddUser(userData))
					{
						System.Web.Security.FormsAuthentication.Authenticate(userData.Login, userData.Password);
						System.Web.Security.FormsAuthentication.SetAuthCookie(userData.Login, true);
						return RedirectToAction("Main", "Match");
					}
					else
					{
						ViewBag.IsExistLogin = true;
					}
				}
			}
			return View();
		}

		[HttpGet]
		public ActionResult Login()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Login([System.Web.Http.FromBody] UserData userData)
		{
			ViewBag.InvalidAuthData = false;
			if (ModelState.IsValid)
			{
				Boolean isUserExists = UserData.CheckUser(userData.Login, userData.Password);
				if (isUserExists)
				{
					System.Web.Security.FormsAuthentication.Authenticate(userData.Login, userData.Password);
					System.Web.Security.FormsAuthentication.SetAuthCookie(userData.Login, true);
					return RedirectToAction("Main", "Match");
				}
				else
				{
					System.Web.Security.FormsAuthentication.SignOut();
					ViewBag.InvalidAuthData = true;
				}
			}
			return View();
		}

		[Authorize]
		[HttpGet]
		public ActionResult SignOut()
		{
			System.Web.Security.FormsAuthentication.SignOut();
			return RedirectToAction("Main", "Match");
		}
	}
}
