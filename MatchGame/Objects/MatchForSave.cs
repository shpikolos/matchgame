﻿using MatchGame.Helpers;
using MatchGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MatchGame.Objects
{
	[Serializable]
	public class MatchForSave
	{
		private const int MAX_SAVED_GAMES_FOR_USER = 5;

		/// <summary>
		/// Пользователь.
		/// </summary>
		public string Login { get; set; }
		/// <summary>
		/// Игры пользователя.
		/// </summary>
		public MatchData[] Games { get; set; }

		public static void Save(string login, MatchData game)
		{
			SaveToXml(login, game);
		}
		public static MatchData[] LoadFromXml(string login)
		{
			return LoadFromXml(login, null);
		}
		public static MatchData LoadFromXml(string login, Guid id)
		{
			MatchData[] games = LoadFromXml(login, null);
			if (games.Length > 0)
			{
				MatchData[] game = (from g in games where g.ID == id select g).ToArray();
				if (game.Length > 0)
					return game[0];
			}
			return null;
		}

		private static void SaveToXml(string login, MatchData game)
		{
			try
			{
				if (MAX_SAVED_GAMES_FOR_USER > 0)
				{
					game.ID = Guid.NewGuid();
					MatchForSave[] data = LoadFromXml();
					List<MatchData> games = LoadFromXml(login, data).ToList();
					games.Insert(0, game);
					if (games.Count > MAX_SAVED_GAMES_FOR_USER)
						games.RemoveAt(games.Count - 1);
					SetData(login, games.ToArray(), ref data);
					string path = GetXmlPath();
					string xml = CommonHelper.Serialize<MatchForSave[]>(data);
					System.IO.File.WriteAllText(path, xml);
				}
			}
			catch
			{
			}
		}
		private static MatchForSave[] LoadFromXml()
		{
			try
			{
				string path = GetXmlPath();
				MatchForSave[] data = CommonHelper.Deserialize<MatchForSave[]>(System.IO.File.ReadAllText(path));
				return data;
			}
			catch
			{
				return new MatchForSave[0];
			}
		}
		private static MatchData[] LoadFromXml(string login, MatchForSave[] data)
		{
			try
			{
				if (data == null)
					data = LoadFromXml();
				MatchData[] result = (from d in data
									  where d.Login == login
									  select d.Games).FirstOrDefault().ToArray();
				return result == null ? new MatchData[0] : result;
			}
			catch
			{
				return new MatchData[0];
			}
		}
		private static void SetData(string login, MatchData[] games, ref MatchForSave[] data)
		{
			MatchForSave gameData = null;
			foreach (MatchForSave gd in data)
			{
				if (gd.Login == login)
				{
					gameData = gd;
					break;
				}
			}
			if (gameData == null)
			{
				gameData = new MatchForSave()
				{
					Login = login,
				};
				List<MatchForSave> list = data.ToList();
				list.Add(gameData);
				data = list.ToArray();
			}
			gameData.Games = games;
		}
		private static string GetXmlPath()
		{
			return System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, @"App_data\SavedGames.xml");
		}
	}
}
